
@section('page_title', 'Details')
@extends('layouts/app')
@section('breadcrumb')
    <h1>
        Details: {!! $book->title !!}
    </h1>
    <ol class="breadcrumb"
    <li><a href="/"><i class="fa fa-dashboard"></i> Tablero</a></li>
    <li><a href="/roles"><i class="fa fa-lock"></i> Books</a></li>
    <li class="active">{!! $book->title !!}</li>
    </ol>
@endsection
@section('content')

    <hr>
    <div class="container-fluid span6">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Book details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        {!! Form::model($book,array('url' => '/book/'.$book->id, 'method' => 'put')) !!}

                        <div class="box-body container-fluid  row">
                            <div class="form-group col-md-6">
                                {!! Form::label('id', '* Id:') !!}
                                {!! Form::text('id', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('author', '* Aurhor:') !!}
                                {!! Form::text('author', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group  col-md-12">
                                {!! Form::label('info', '* Info:') !!}
                                {!! Form::text('info', null, ['class' => 'form-control']) !!}
                            </div>

                            <hr>

                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}
                        <div class="form-group col-md-12">
                            <a href="/">
                            <button class="btn btn-success" id="">

                                Back

                            </button>
                            </a>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>



@endsection

@section('styles')

    {!! Html::style('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css',array('media'=>'screen,projection')) !!}

@endsection

@section('scripts')

    {!! Html::script(asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")) !!}
    {!! Html::script(asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")) !!}

    {!! Html::script(asset("/js/app/index_roles.js")) !!}

@endsection