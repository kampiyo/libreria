@section('page_title', $book->title)
@extends('layouts/app')
@section('breadcrumb')
    <h1>
        Editar: {!! $book->title !!}
    </h1>
    <ol class="breadcrumb"
        <li><a href="/"><i class="fa fa-dashboard"></i> Tablero</a></li>
        <li><a href="/roles"><i class="fa fa-lock"></i> Roles</a></li>
        <li class="active">{!! $book->title !!}</li>
    </ol>
@endsection
@section('content')

    <hr>
    <div class="container-fluid span6">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit book</h3>
                        </div>
                        <!-- /.box-header -->

                        {!! Form::model($book,array('url' => '/book/'.$book->id, 'method' => 'put')) !!}

                        <div class="box-body container-fluid  row">
                            <div class="form-group col-md-6">
                                {!! Form::label('author', '* Author:') !!}
                                {!! Form::text('author', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('title', '* Title:') !!}
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group  col-md-6">
                                {!! Form::label('info', '* Info:') !!}
                                {!! Form::text('info', null, ['class' => 'form-control']) !!}
                            </div>

                            <hr>
                            <div class="form-group col-md-12">
                                <button class="btn btn-success" id="">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection
@section('styles')
@endsection
@section('scripts')
@endsection