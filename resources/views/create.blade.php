
@section('page_title', 'Books')
@extends('layouts/app')
@section('breadcrumb')
@endsection
@section('content')


    <hr>
    <div class="container-fluid span6">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">New book</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        {!! Form::open(array('url' => '/book')) !!}

                        <div class="box-body container-fluid  row">
                            <div class="form-group col-md-6">
                                {!! Form::label('author', '* Author:') !!}
                                {!! Form::text('author', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('title', '* Title:') !!}
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::label('info', '* Info') !!}
                                {!! Form::text('info', null, ['class' => 'form-control']) !!}
                            </div>

                            <hr>
                            <div class="form-group col-md-12">
                                <button class="btn btn-success" id="">
                                    Register
                                </button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>


@endsection

@section('styles')

    {!! Html::style('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css',array('media'=>'screen,projection')) !!}

@endsection

@section('scripts')

    {!! Html::script(asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")) !!}
    {!! Html::script(asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")) !!}

    {!! Html::script(asset("/js/app/index_roles.js")) !!}

@endsection